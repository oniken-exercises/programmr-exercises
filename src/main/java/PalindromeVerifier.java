import java.util.Arrays;
import java.util.logging.Logger;

/**
 * @author Cronut
 * This class verifies if a string is a palindrome or not. A palindrome is defined as a chain of chars,
 * which read backwards is the same as read forwards.
 * @see <a href="http://www.programmr.com/challenges/palindrom-string-2">Programmr Exercise: Palindrome</a>
 */
class PalindromeVerifier {

    private Logger log = Logger.getLogger(getClass().getSimpleName());
    char temporaryValue;

    /**
     * Checks if the passed parameter is a valid palindrome. The passed parameter will be reduced about its special
     * characters and spaces.
     * @param testString
     * @return True if the passed parameter is a valid palindrome, otherwise false.
     */
    public boolean isPalindrome(String testString) {
        log.info("String to verify: " + testString);

        testString = extractWordCharacters(testString);
        if(isNonNullAndMinCharString(testString))
        {   return false;   }

        char[] loweredTestChars = testString.toCharArray();
        log.info("Before position switching: " + Arrays.toString(loweredTestChars));
        // also possible with the reverse function of StringBuilder
        for(int i = 0, j = loweredTestChars.length-1;
            i < (loweredTestChars.length-1)/2;
            i++, j--) {
            switchCharPositions(loweredTestChars, i, j);
        }
        log.info("After position switching: " + Arrays.toString(loweredTestChars));
        return String.copyValueOf(loweredTestChars).equals(testString);
    }

    /**
     * This method solves the given exercise in the right way.
     * @param testString
     * @return
     */
    public String textualResultIsPalindrome(String testString) {
        if(isPalindrome(testString)) {
            return "palindrome";
        }
        else return "not a palindrome";
    }

    private boolean isNonNullAndMinCharString(String testString) {
        return (testString == null || testString.length() < 2);
    }

    private void switchCharPositions(char[] loweredTestChars, int i, int j) {
        temporaryValue = loweredTestChars[i];
        loweredTestChars[i] = loweredTestChars[j];
        loweredTestChars[j] = temporaryValue;
    }

    private String extractWordCharacters(String testString) {
        return testString.replaceAll("\\W", "").toLowerCase();
    }
}
