import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Logger;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author Cronut
 */
@RunWith(Parameterized.class)
public class PalindromeVerifierTest {

    @Parameterized.Parameters
    public static Collection<Object[]> testParameter() {
        return Arrays.asList(new Object[][] {
                { "otto", true }, { "jakob", false }, { "Tacocat", true }, { "A nut for a jar of tuna", true },
                { "Dreh mal am Herd!", true }, { "A", false }
        });
    }

    @Parameterized.Parameter
    public String testString;
    @Parameterized.Parameter(1)
    public boolean expectedVerifierResult;

    public void testPalindromes(String testString, boolean expectedVerifierResult) {
        this.testString = testString;
        this.expectedVerifierResult = expectedVerifierResult;
    }

    @Test
    public void testIsPalindrome() {
        assertThat(new PalindromeVerifier().isPalindrome(testString), is(expectedVerifierResult));
    }
}
